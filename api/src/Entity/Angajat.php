<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\AngajatRepository;
use Doctrine\ORM\Mapping as ORM;

use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: AngajatRepository::class)]
#[ApiResource(
    normalizationContext: ['groups' => ['read']],
    denormalizationContext: ['groups' => ['write']],
    itemOperations: [
        'put' ,
        'delete'
    ],
    collectionOperations: [
        'post'
        
        
    ]
)]
class Angajat
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[Groups(['read','write'])]
    #[ORM\Column(type: 'string', length: 255)]
    private $nume;

    #[Groups(['read','write'])]
    #[ORM\OneToOne(mappedBy: 'relatiiAngajat', targetEntity: Depozit::class, cascade: ['persist', 'remove'])]
    private $relatiiDepozit;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNume(): ?string
    {
        return $this->nume;
    }

    public function setNume(string $nume): self
    {
        $this->nume = $nume;

        return $this;
    }

    public function getRelatiiDepozit(): ?Depozit
    {
        return $this->relatiiDepozit;
    }

    public function setRelatiiDepozit(?Depozit $relatiiDepozit): self
    {
        // unset the owning side of the relation if necessary
        if ($relatiiDepozit === null && $this->relatiiDepozit !== null) {
            $this->relatiiDepozit->setRelatiiAngajat(null);
        }

        // set the owning side of the relation if necessary
        if ($relatiiDepozit !== null && $relatiiDepozit->getRelatiiAngajat() !== $this) {
            $relatiiDepozit->setRelatiiAngajat($this);
        }

        $this->relatiiDepozit = $relatiiDepozit;

        return $this;
    }
}
