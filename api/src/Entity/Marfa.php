<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\MarfaRepository;
use Doctrine\ORM\Mapping as ORM;

use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: MarfaRepository::class)]
#[ApiResource(
    normalizationContext: ['groups' => ['read']],
    denormalizationContext: ['groups' => ['write']],
    itemOperations: [
        
    ],
    collectionOperations: [
        'post'
    ]
)]
class Marfa
{
    
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[Groups(['read','write'])]
    #[ORM\Column(type: 'string', length: 255)]
    private $nume;

    #[Groups(['read','write'])]
    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $descriere;

    #[Groups(['read','write'])]
    #[ORM\Column(type: 'string', length: 255)]
    private $dataExpirari;

    #[Groups(['read','write'])]
    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $fragil;

    #[Groups(['read','write'])]
    #[ORM\Column(type: 'integer')]
    private $greutate;

    #[Groups(['read','write'])]
    #[ORM\Column(type: 'integer')]
    private $volum;

    #[Groups(['read','write'])]
    #[ORM\ManyToOne(targetEntity: Depozit::class, inversedBy: 'relatiiMarfa')]
    private $relatiiDepozit;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNume(): ?string
    {
        return $this->nume;
    }

    public function setNume(string $nume): self
    {
        $this->nume = $nume;

        return $this;
    }

    public function getDescriere(): ?string
    {
        return $this->descriere;
    }

    public function setDescriere(?string $descriere): self
    {
        $this->descriere = $descriere;

        return $this;
    }

    public function getDataExpirari(): ?string
    {
        return $this->dataExpirari;
    }

    public function setDataExpirari(string $dataExpirari): self
    {
        $this->dataExpirari = $dataExpirari;

        return $this;
    }

    public function getFragil(): ?string
    {
        return $this->fragil;
    }

    public function setFragil(?string $fragil): self
    {
        $this->fragil = $fragil;

        return $this;
    }

    public function getGreutate(): ?int
    {
        return $this->greutate;
    }

    public function setGreutate(int $greutate): self
    {
        $this->greutate = $greutate;

        return $this;
    }

    public function getVolum(): ?int
    {
        return $this->volum;
    }

    public function setVolum(int $volum): self
    {
        $this->volum = $volum;

        return $this;
    }

    public function getRelatiiDepozit(): ?Depozit
    {
        return $this->relatiiDepozit;
    }

    public function setRelatiiDepozit(?Depozit $relatiiDepozit): self
    {
        $this->relatiiDepozit = $relatiiDepozit;

        return $this;
    }
}
