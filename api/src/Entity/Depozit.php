<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\DepozitRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: DepozitRepository::class)]
#[ApiResource(
    normalizationContext: ['groups' => ['read']],
    denormalizationContext: ['groups' => ['write']],
    itemOperations: [
        'get' => ['normalization_context' => ['groups' => ['get']]],
        'put'
    ],
    collectionOperations: [
        
        'post'
    ]
)]
class Depozit
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[Groups(['read','write'])]
    #[ORM\Column(type: 'string', length: 255)]
    private $nume;

    #[Groups(['read','write'])]
    #[ORM\Column(type: 'string', length: 255)]
    private $locatie;

    #[Groups(['read','write'])]
    #[ORM\Column(type: 'date')]
    private $dataIntrare;

    #[Groups(['read','write'])]
    #[ORM\Column(type: 'date', nullable: true)]
    private $dataIesire;

    #[Groups(['read','write'])]
    #[ORM\Column(type: 'string', length: 255)]
    private $relatieMarfa;

    #[Groups(['read','write'])]
    #[ORM\OneToMany(mappedBy: 'relatiiDepozit', targetEntity: Marfa::class)]
    private $relatiiMarfa;

    #[Groups(['read','write'])]
    #[ORM\Column(type: 'string', length: 255)]
    private $Angajat;

    #[Groups(['read','write'])]
    #[ORM\OneToOne(inversedBy: 'relatiiDepozit', targetEntity: Angajat::class, cascade: ['persist', 'remove'])]
    private $relatiiAngajat;

    public function __construct()
    {
        $this->relatiiMarfa = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNume(): ?string
    {
        return $this->nume;
    }

    public function setNume(string $nume): self
    {
        $this->nume = $nume;

        return $this;
    }

    public function getLocatie(): ?string
    {
        return $this->locatie;
    }

    public function setLocatie(string $locatie): self
    {
        $this->locatie = $locatie;

        return $this;
    }

    public function getDataIntrare(): ?\DateTimeInterface
    {
        return $this->dataIntrare;
    }

    public function setDataIntrare(\DateTimeInterface $dataIntrare): self
    {
        $this->dataIntrare = $dataIntrare;

        return $this;
    }

    public function getDataIesire(): ?\DateTimeInterface
    {
        return $this->dataIesire;
    }

    public function setDataIesire(?\DateTimeInterface $dataIesire): self
    {
        $this->dataIesire = $dataIesire;

        return $this;
    }

    public function getRelatieMarfa(): ?string
    {
        return $this->relatieMarfa;
    }

    public function setRelatieMarfa(string $relatieMarfa): self
    {
        $this->relatieMarfa = $relatieMarfa;

        return $this;
    }

    /**
     * @return Collection<int, Marfa>
     */
    public function getRelatiiMarfa(): Collection
    {
        return $this->relatiiMarfa;
    }

    public function addRelatiiMarfa(Marfa $relatiiMarfa): self
    {
        if (!$this->relatiiMarfa->contains($relatiiMarfa)) {
            $this->relatiiMarfa[] = $relatiiMarfa;
            $relatiiMarfa->setRelatiiDepozit($this);
        }

        return $this;
    }

    public function removeRelatiiMarfa(Marfa $relatiiMarfa): self
    {
        if ($this->relatiiMarfa->removeElement($relatiiMarfa)) {
            // set the owning side to null (unless already changed)
            if ($relatiiMarfa->getRelatiiDepozit() === $this) {
                $relatiiMarfa->setRelatiiDepozit(null);
            }
        }

        return $this;
    }

    public function getAngajat(): ?string
    {
        return $this->Angajat;
    }

    public function setAngajat(string $Angajat): self
    {
        $this->Angajat = $Angajat;

        return $this;
    }

    public function getRelatiiAngajat(): ?Angajat
    {
        return $this->relatiiAngajat;
    }

    public function setRelatiiAngajat(?Angajat $relatiiAngajat): self
    {
        $this->relatiiAngajat = $relatiiAngajat;

        return $this;
    }
}
